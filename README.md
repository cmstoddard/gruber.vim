![Screenshot](https://i.imgur.com/nYjIn5T.png)

Ported to Vim and slightly modified [Gruber Dark](https://daringfireball.net/projects/bbcolors/schemes/) theme for BBEdit by John Gruber. The colors have been adjusted to match Xterm 256 color palette.

1. Install theme using plugin manager or copy manually to your `~/.vim/colors` (`~/.config/nvim/colors` for Neovim).
2. Apply theme in Vim by `:colorscheme gruber`.
